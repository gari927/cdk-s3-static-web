# cdk-s3-static-web

## 参考
https://dev.classmethod.jp/cloud/aws-cdk-s3-deploy/

$ pip install --upgrade pip  
$ pip install -r requirements.txt  
$ pip install aws_cdk.aws-s3 aws_cdk.aws-s3-deployment  
  
## メリット

- S3ホスティングを使用して簡単にWebサイトをデプロイできる
- 作られたユーザーそれぞれは特定のフォルダ以下でしか作業ができないように制御されている
- CDKを使用することで、他のAWSアカウントでも同様の環境を簡単に作成することが可能
- user.jsonを使用することでuser数をコントロールすることが可能

## 課題

- s3バケットはy全世界で一意なので気を付ける
- ログインするにはCyberDuckやWinSCPなどのs3に対応したツールが必要となる
- 各自のAccessKeyとSecretKeyをemail等で送りつけたいが未対応
- ツールの制約上、他のバケットも中身は見れないが一覧表示で見れてしまう

## 使用例

- CyberDuckからs3を選択し、
```
アクセスキーID:AKIAVOUSVIKSBOR3DTEY
シークレットアクセスキー:XkjSIUOC2vhWDvvMNuLeBA6yLQ/gIa3+gtbeGyBL
```
を入力。バケットから
```
student-web-site
```
を選択
student1はアクセスできるが、student2はアクセスできないことを確認する。

