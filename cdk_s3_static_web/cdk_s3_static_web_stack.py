import json

from aws_cdk import (
    aws_s3_deployment as s3deploy,
    aws_s3 as s3,
    aws_iam,
    core,
)


class CdkS3StaticWebStack(core.Stack):

    def __init__(self, scope: core.Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # ステージング用S3バケットの作成
        website_bucket = s3.Bucket(self, 'website_bucket',
                                   bucket_name="student-web-site-2020",
                                   website_index_document='index.html',
                                   public_read_access=True,
                                   removal_policy=core.RemovalPolicy.DESTROY
                                   )

        # 生徒グループの作成
        group = aws_iam.Group(self, 'iamGroup',
                              group_name='student_group',
                              )

        self.setIamPolicy(self, website_bucket, group)

        access_key_list = []
        with open('./user.json') as f:
            jsn = json.load(f)
            for jsn_val in jsn.values():
                user_id = jsn_val["id"]
                access_key = self.createUser(self, user_id, website_bucket, group)

                access_key_list.append({
                    "secret_access_key": access_key.attr_secret_access_key,
                    "user_name": access_key.user_name,
                    "access_key": access_key,
                    "mail": jsn_val["mail"],
                })

                core.CfnOutput(
                    self, id=user_id + "user_name", value=access_key.user_name
                )
                core.CfnOutput(
                    self, id=user_id + "access_key", value=access_key.ref
                )
                core.CfnOutput(
                    self, id=user_id + "secret_access_key", value=access_key.attr_secret_access_key
                )
                core.CfnOutput(
                    self, id=user_id + "mail", value=jsn_val["mail"]
                )

        core.CfnOutput(
            self, id="bucket_website_url", value=website_bucket.bucket_website_url
        )

    @staticmethod
    def setIamPolicy(self, bucket, group):
        statement1 = aws_iam.PolicyStatement(
            actions=[
                "s3:ListAllMyBuckets",
                "s3:GetBucketLocation",
            ],
            effect=aws_iam.Effect.ALLOW,
            resources=[
                "arn:aws:s3:::*",
            ]
        )

        statement2 = aws_iam.PolicyStatement(
            actions=[
                "s3:ListBucket",
            ],
            effect=aws_iam.Effect.ALLOW,
            resources=[
                bucket.bucket_arn,
            ],
            conditions={'StringEquals': {'s3:prefix': ["", "web/"], 's3:delimiter': ["/"]}}
        )

        statement3 = aws_iam.PolicyStatement(
            actions=[
                "s3:ListBucket",
            ],
            effect=aws_iam.Effect.ALLOW,
            resources=[
                bucket.bucket_arn,
            ],
            conditions={'StringLike': {'s3:prefix': ["${aws:username}/*"]}}
        )
        statement4 = aws_iam.PolicyStatement(
            actions=[
                "s3:*",
            ],
            effect=aws_iam.Effect.ALLOW,
            resources=[
                f'{bucket.bucket_arn}' + "/${aws:username}/*",
            ],
            conditions={'StringLike': {'s3:prefix': ["${aws:username}/*"]}}
        )
        statement5 = aws_iam.PolicyStatement(
            actions=[
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:PutObject"
            ],
            effect=aws_iam.Effect.ALLOW,
            resources=[
                f'{bucket.bucket_arn}' + "/${aws:username}/*",
            ]
        )

        group.add_to_policy(statement1)
        group.add_to_policy(statement2)
        group.add_to_policy(statement3)
        group.add_to_policy(statement4)
        group.add_to_policy(statement5)

    @staticmethod
    def createUser(self, user_name, website_bucket, group):
        user = aws_iam.User(self, id="user_" + user_name, groups=[group], user_name=user_name)
        access_key = aws_iam.CfnAccessKey(self, id="AccessKey_" + user_name, user_name=user.user_name)

        # ローカルファイルのデプロイ
        s3deploy.BucketDeployment(self, "DeployWebsite_" + user_name,
                                  sources=[s3deploy.Source.asset("./website-src")],
                                  destination_bucket=website_bucket,
                                  destination_key_prefix=user_name
                                  )

        return access_key
