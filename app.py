#!/usr/bin/env python3

from aws_cdk import core

from cdk_s3_static_web.cdk_s3_static_web_stack import CdkS3StaticWebStack


app = core.App()
CdkS3StaticWebStack(app, "cdk-s3-static-web")

app.synth()
